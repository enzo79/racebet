# ************************************************************
# RACEBET API dump
# Host: 127.0.0.1 (MySQL 5.5.49-0+deb8u1)
# Database: racebetapi
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

drop database IF EXISTS racebetapi;
CREATE DATABASE racebetapi CHARACTER SET utf8 COLLATE utf8_general_ci;
use racebetapi;

# Dump of table balance
# ------------------------------------------------------------

DROP TABLE IF EXISTS `balance`;

CREATE TABLE `balance` (
  `id_balance` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fk_customer` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `bonus` int(11) NOT NULL,
  `deposit` int(11) NOT NULL,
  `withdrawal` int(11) DEFAULT NULL,
  `total_deposit` int(11) NOT NULL,
  `total_withdrawal` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_balance`),
  UNIQUE KEY `fk_customer` (`fk_customer`),
  KEY `fk_customer_2` (`fk_customer`),
  CONSTRAINT `fk_customer_2` FOREIGN KEY (`fk_customer`) REFERENCES `customer` (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) NOT NULL,
  `first_name` varchar(250) NOT NULL DEFAULT '',
  `last_name` varchar(250) NOT NULL DEFAULT '',
  `gender` char(6) NOT NULL,
  `country` char(2) NOT NULL DEFAULT '',
  `bonus` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_customer`),
  KEY `email_I` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table operation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operation`;

CREATE TABLE `operation` (
  `id_operation` int(11) NOT NULL AUTO_INCREMENT,
  `fk_customer` int(11) NOT NULL,
  `fk_operation_type` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `bonus` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_operation`),
  KEY `fk_operation_type_1` (`fk_operation_type`),
  KEY `fk_customer_1` (`fk_customer`),
  CONSTRAINT `fk_customer_1` FOREIGN KEY (`fk_customer`) REFERENCES `customer` (`id_customer`),
  CONSTRAINT `fk_operation_type_1` FOREIGN KEY (`fk_operation_type`) REFERENCES `operation_type` (`id_operation_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table operation_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operation_type`;

CREATE TABLE `operation_type` (
  `id_operation_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_operation_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `operation_type` WRITE;
/*!40000 ALTER TABLE `operation_type` DISABLE KEYS */;

INSERT INTO `operation_type` (`id_operation_type`, `name`)
VALUES
  (1,'deposit'),
  (2,'withdrawal');

/*!40000 ALTER TABLE `operation_type` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
