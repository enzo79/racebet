#!/usr/bin/env bash

if [ -z $1 ] || [ $1 != "vagrant" ]
then
   echo 'This script should be executed by Vagrant on developers machines only!'
   exit 1
fi
    echo "Initialize Racebet api database"

    VAGRANTDIR="/data/sources/racebet-api/"

    mysql -h127.0.0.1 -uroot -p1234 < ${VAGRANTDIR}.provision/config/database/racebetapi.sql

    echo "Done"

echo