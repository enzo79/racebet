#!/usr/bin/env bash

if [ -z $1 ] || [ $1 != "vagrant" ]
then
   echo 'This script should be executed by Vagrant on developers machines only!'
   exit 1
fi

    VAGRANTDIR="/data/sources/racebet-api/"

    sudo apt-get update

    #setup utilities
    echo "Installing unzip"
    sudo apt-get install unzip -y > /dev/null

    # setup nginx
    echo "Installing nginx"
    sudo apt-get install nginx -y > /dev/null

    # setup mysql
    echo "Installing mysql"

    debconf-set-selections <<< "mysql-server mysql-server/root_password password 1234"
    debconf-set-selections <<< "mysql-server mysql-server/root_password_again password 1234"
    sudo apt-get install mysql-server -y > /dev/null
    sudo apt-get install mysql-client > /dev/null

    # setup php7
    echo "installing php7 dependencies"

    # Dependencies
    sudo apt-get update
    sudo apt-get install -y \
    build-essential \
    pkg-config \
    git-core \
    autoconf \
    bison \
    libxml2-dev \
    libbz2-dev \
    libmcrypt-dev \
    libicu-dev \
    libssl-dev \
    libcurl4-openssl-dev \
    libltdl-dev \
    libjpeg-dev \
    libpng-dev \
    libpspell-dev \
    libreadline-dev

    sudo mkdir -p /usr/local/php7

    echo "installing php7"
    wget -O php7.zip https://github.com/php/php-src/archive/php-7.0.7.zip

    sudo rm -rf php-src-php-7.0.7/
    unzip php7.zip

    cd php-src-php-7.0.7/

    ./buildconf --force

    CONFIGURE_STRING="--prefix=/usr/local/php7 \
    --with-config-file-scan-dir=/usr/local/php7/etc/conf.d \
                  --without-pear \
                  --enable-bcmath \
                  --with-bz2 \
                  --enable-calendar \
                  --enable-intl \
                  --enable-exif \
                  --enable-dba \
                  --enable-ftp \
                  --with-gettext \
                  --with-gd \
                  --with-jpeg-dir \
                  --enable-mbstring \
                  --with-mcrypt \
                  --with-mhash \
                  --enable-mysqlnd \
                  --with-mysql=mysqlnd \
                  --with-mysql-sock=/var/run/mysqld/mysqld.sock \
                  --with-mysqli=mysqlnd \
                  --with-pdo-mysql=mysqlnd \
                  --with-openssl \
                  --enable-pcntl \
                  --with-pspell \
                  --enable-shmop \
                  --enable-soap \
                  --enable-sockets \
                  --enable-sysvmsg \
                  --enable-sysvsem \
                  --enable-sysvshm \
                  --enable-wddx \
                  --with-zlib \
                  --enable-zip \
                  --with-readline \
                  --with-curl \
                  --enable-fpm \
                  --with-fpm-user=www-data \
                  --with-fpm-group=www-data"

    sudo ./configure $CONFIGURE_STRING

    sudo make
    sudo make install

    # Create a dir for storing PHP module conf
    sudo mkdir -p /usr/local/php7/etc/conf.d

    # Symlink php-fpm to php7-fpm
     if [ -f /usr/local/php7/sbin/php7-fpm ]; then
        sudo rm  /usr/local/php7/sbin/php7-fpm
    fi
    sudo ln -s /usr/local/php7/sbin/php-fpm /usr/local/php7/sbin/php7-fpm

    # Add config files
    sudo cp php.ini-production /usr/local/php7/lib/php.ini

    #xDebug

    cd /tmp/
    echo "installing php7"
    sudo wget  https://xdebug.org/files/xdebug-2.4.0.tgz
    sudo rm -rf xdebug-2.4.0/

    sudo tar -xvzf xdebug-2.4.0.tgz

    cd xdebug-2.4.0/

    sudo /usr/local/php7/bin/phpize
    sudo ./configure --enable-xdebug --with-php-config=/usr/local/php7/bin/php-config
    sudo make

    sudo cp modules/xdebug.so /usr/local/php7/lib/php/.

    cd ${VAGRANTDIR}.provision

    sudo cp config/php7/www.conf /usr/local/php7/etc/php-fpm.d/www.conf
    sudo cp config/php7/php-fpm.conf /usr/local/php7/etc/php-fpm.conf
    sudo cp config/php7/modules.ini /usr/local/php7/etc/conf.d/modules.ini

    # Add the init script
    sudo cp config/php7/php7-fpm.init /etc/init.d/php7-fpm
    sudo chmod +x /etc/init.d/php7-fpm
    sudo update-rc.d php7-fpm defaults

    sudo service php7-fpm start

    # setup nginx
    echo "setting virtual host for racebet-api"

    sudo mkdir -p /data/logs/development

    sudo cp config/nginx/upstream  /etc/nginx/sites-available/upstream

    if [ -f /etc/nginx/sites-enabled/upstream ]; then
        sudo rm /etc/nginx/sites-enabled/upstream
    fi
    sudo ln -s /etc/nginx/sites-available/upstream /etc/nginx/sites-enabled/upstream


    sudo cp config/nginx/racebet-api /etc/nginx/sites-available/racebet-api

    if [ -f /etc/nginx/sites-enabled/racebet-api ]; then
        sudo rm /etc/nginx/sites-enabled/racebet-api
    fi

    sudo ln -s /etc/nginx/sites-available/racebet-api /etc/nginx/sites-enabled/racebet-api
    sudo rm  /etc/nginx/sites-enabled/default

    echo "restarting nginx"
    sudo service nginx restart

    cd /
    # install composer globaly

    echo "installing composer globally"

    /usr/local/php7/bin/php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
    /usr/local/php7/bin/php composer-setup.php
    /usr/local/php7/bin/php -r "unlink('composer-setup.php');"
    sudo mv composer.phar /usr/local/bin/.
    sudo cp ${VAGRANTDIR}.provision/config/php7/composer /usr/local/bin/.
    sudo chmod 755 /usr/local/bin/composer
    sudo chmod 755 /usr/local/bin/composer.phar

    echo "Racebet-api: Done"

echo