<?php

return [
    'dbsettings'  => $dbSettings,

    'logger' => [
        'file'  => $loggerFile,
        'level' => $loggerLevel,
    ],
    
    'bonus' => [
        'min' => 5,
        'max' => 20,
    ]

];
