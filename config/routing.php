<?php

use Symfony\Component\HttpFoundation\Request;

$app->before(function (Request $request) use ($app) {

    if ($request->headers->get('Content-Type') === 'application/json') {
        $data = json_decode($request->getContent(), true);
        $request->request->replace((array)$data);
    }

});

$app->get('/ping', function () use ($app) {
        return $app['controllers.index']
            ->pingAction($app['services.pingService']);
});

// CUSTOMER ROUTES
$app->get('/customer', function (Request $request) use ($app) {
        return $app['controllers.customer']
            ->customersAction($request, $app['services.customer']);
});

$app->get('/customer/{id}', function (Request $request) use ($app) {
    return $app['controllers.customer']
        ->customerAction($request, $app['services.customer']);
});


$app->post('/customer', function (Request $request) use ($app) {
    return $app['controllers.customer']
        ->updateCustomerAction($request, $app['services.customer']);
});

$app->put('/customer', function (Request $request) use ($app) {
    return $app['controllers.customer']
        ->createCustomerAction($request, $app['services.customer']);
});

// FINANCIAL ROUTES
$app->put('/deposit', function (Request $request) use ($app) {
    return $app['controllers.financial']
        ->depositAction($request, $app['services.financial']);
});

$app->put('/withdrawal', function (Request $request) use ($app) {
    return $app['controllers.financial']
        ->withdrawalAction($request, $app['services.financial']);
});

$app->get('/reports', function (Request $request) use ($app) {
    return $app['controllers.financial']
        ->defaultReportsAction($request, $app['services.financial']);
});

$app->get('/reports/{start_date}/{end_date}', function (Request $request) use ($app) {
    return $app['controllers.financial']
        ->reportsAction($request, $app['services.financial']);
});


$app->get('/balance/{id_customer}', function (Request $request) use ($app) {
    return $app['controllers.financial']
        ->getBalanceAction($request, $app['services.financial']);
});