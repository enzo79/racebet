<?php

/**
 * @var \Racebet\Api\App $app
 */

/* Internals */

use Csanquer\Silex\PdoServiceProvider\Provider\PDOServiceProvider;

$app['config']     = json_decode(file_get_contents(__DIR__. '/config.json'), false);
$app['pdo.prefix'] = 'pdo';
$dbSettings = (array)$app['config']->dbsettings;
$bonusSettings = (array)$app['config']->bonus;

/* Services */
$app->register(new Silex\Provider\ValidatorServiceProvider());

$app->register(
    new PDOServiceProvider($app['pdo.prefix']),
    $dbSettings
);

$app['services.pingService'] = $app->share(function () {
    return new Racebet\Api\Kernel\Service\Ping();
});

$app['services.customer'] = $app->share(function () use ($app) {
    return new Racebet\Api\Kernel\Service\Customer(
        $app['models.customer'],
        $app['services.validator.customer']
    );
});

$app['services.customer'] = $app->share(function () use ($app) {
    return new Racebet\Api\Kernel\Service\Customer(
        $app['models.customer'],
        $app['services.validator.customer']
    );
});

$app['services.financial'] = $app->share(function () use ($app) {
    return new Racebet\Api\Kernel\Service\Financial(
        $app['models.customer'],
        $app['services.validator.financial']
    );
});


$app['services.validator.customer'] = $app->share(function () use ($app) {
    return new Racebet\Api\Kernel\Validator\Customer(
        $app['validator']
    );
});

$app['services.validator.financial'] = $app->share(function () use ($app) {
    return new Racebet\Api\Kernel\Validator\Financial(
        $app['validator']
    );
});

/* Models */

$app['models.customer'] = $app->share(function () use ($app, $bonusSettings) {
    return new Racebet\Api\Kernel\Model\Customer(
        $app['pdo'],
        $bonusSettings
    );
});

/* Controllers */

$app['controllers.index'] = $app->share(function () {
    return new Racebet\Api\Controller\Index();
});

$app['controllers.financial'] = $app->share(function () {
    return new Racebet\Api\Controller\Financial();
});

$app['controllers.customer'] = $app->share(function () {
    return new Racebet\Api\Controller\Customer();
});

