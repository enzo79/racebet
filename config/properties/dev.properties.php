<?php

$loggerFile = '/var/log/racebet-api.log';
$loggerLevel = 100;

/**
 * DB configuration
 */

$dbSettings =
    [
        'pdo.server'   => [
            // PDO driver to use among : mysql, pgsql , oracle, mssql, sqlite, dblib
            'driver'   => 'mysql',
            'host'     => '127.0.0.1',
            'dbname'   => 'racebetapi',
            'port'     => 3306,
            'user'     => 'root',
            'password' => '1234',
        ],
        // optional PDO attributes used in PDO constructor 4th argument driver_options
        // some PDO attributes can be used only as PDO driver_options
        // see http://www.php.net/manual/fr/pdo.construct.php
        'pdo.options' => [
            \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
        ],
        // optional PDO attributes set with PDO::setAttribute
        // see http://www.php.net/manual/fr/pdo.setattribute.php
        'pdo.attributes' => [
            \PDO::ATTR_STRINGIFY_FETCHES => false,
        ],
    ];
