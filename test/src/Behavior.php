<?php
use PhpSpec\ObjectBehavior;

class Behavior extends ObjectBehavior
{
    private static $app = null;

    public function getApp()
    {
        if (!self::$app) {
            self::$app = $app = new Racebet\Api\App();

            require PROJECT_BASE_DIR. '/config/di.php';
        }

        return self::$app;
    }
}