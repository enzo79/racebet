<?php

namespace spec\Racebet\Api;

use PhpSpec\ObjectBehavior;

class AppSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Racebet\Api\App');
    }
}
