<?php

namespace spec\Racebet\Api\Controller;

use Racebet\Api\Kernel\Service\Ping;

class IndexSpec extends \Behavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Racebet\Api\Controller\Index');
    }

    function it_should_return_correct_ping_response(Ping $pingService)
    {
        $pingService->getOK()->willReturn((array)json_decode('{"status":"ok","message":""}', false));
        $this->pingAction($pingService)->getContent()->shouldReturn('{"status":"ok","message":""}');
    }
}
