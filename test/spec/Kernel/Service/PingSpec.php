<?php

namespace spec\Racebet\Api\Kernel\Service;


class PingSpec extends \Behavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Racebet\Api\Kernel\Service\Ping');
    }

    function it_should_return_ok()
    {
        $this->getOK()->shouldReturn((array)json_decode('{"status":"ok","message":""}', false));
    }
}
