<?php

namespace %namespace%;

use Prophecy\Argument;

class %name% extends \Behavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('%subject%');
    }
}
