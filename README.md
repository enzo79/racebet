# Racebet Api

### Description
#### Environment
* Debian "jessie" 8
* nginx
* php7
* mysql

#### Technologies
* Silex 1.3
* composer
* php PDO
* PhpSpec
* git
* Vagrant

### installation

#### Vagrant
* Download Vagrant from https://releases.hashicorp.com/vagrant/1.8.0/
* Install Vagrant on your local machine
* On your local machine
```sh
run vagrant plugin install vagrant-hostmanager
```

#### Virtualbox
* Download Virtualbox from https://www.virtualbox.org/wiki/Downloads
* Install Virtualbox

#### Racebet Api setup
 * Clone racebet repository
```sh
git clone https://enzo79@bitbucket.org/enzo79/racebet.git
```
* Setup Racebet Api
```sh
cd racebet
vagrant up
```
* connect to Racebet virtual machine and run build.sh which run composer update to get all the needed libraries and creates a json configiguration file.
```sh
ssh vagrant@192.168.100.103 password : vagrant
cd /data/sources/racebet-api
ENV=dev ./build.sh
```
#### Documentation
* A representation of racebet RESTful API is provided using Swagger http://racebet-api.io/doc/ (http://swagger.io/) which can be used to test the api as well.

##### Endpoints
* Ping service http://racebet-api.io/ping - method GET
* Customer listing http://racebet-api.io/customer - method GET
* Customer detail http://racebet-api.io/customer/{id_customer} - method GET
* Customer create http://racebet-api.io/customer - method PUT
payload
```javascripttest
{
  "customer": {
    "email": "test@kdkk.it",
    "first_name": "name",
    "last_name": "last name",
    "gender": "Male",
    "country": "IT"
  }
}
```
* Customer update http://racebet-api.io/customer - method POST
payload
```javascripttest
{
  "customer": {
    "id_customer" : 1,
    "email": "test2@kdkk.it",
    "first_name": "name2",
    "last_name": "last name2",
    "gender": "Female",
    "country": "DE"
  }
}
```
* Deposit http://racebet-api.io/deposit - method PUT
payload
```javascripttest
{
  "operation": {
    "id_customer": 1,
    "amount": 2300
  }
}
```
* Withdrawal http://racebet-api.io/withdrawal - method PUT
payload
```javascripttest
{
  "operation": {
    "id_customer": 1,
    "amount": 2300
  }
}
```
* Customer balance http://racebet-api.io/balance/{id_customer} - method GET
* Reports http://racebet-api.io/reports/{days} - method GET

#### Test Framework
* PHPSpec is the test framework used. Only some test is provided so not all the code is test covered.
To run run the tests:
```sh
cd <PROJECT_DIRECTORY>/tests
/usr/local/php7/bin/php phpspec run
```