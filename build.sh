#!/usr/bin/env bash

declare -a ENVS=("dev" "live" "test" "staging");
composerArguments = '--optimize-autoloader'

if [[ (-z $ENV) || (! ${ENVS[*]} =~ $ENV) ]]
then
   echo 'Valid environment parameter (ENV) required'
   exit
fi

if [[ $ENV != "dev" ]]
then
    composerArguments="$composerArguments --no-dev"
fi

echo
echo "Building project Racebet-api"
echo

#git pull
composer install $composerArguments

/usr/local/php7/bin/php -r "
    require_once 'config/properties/$ENV.properties.php';
    \$config = require_once 'config/config_template.php';
    echo json_encode(\$config);
" > config/config.json
