<?php

namespace Racebet\Api;

use Silex\Application;

class App extends Application
{
    /**
     * @return bool
     */
    public function authorize() : bool
    {
        //implement authorization here if you need one
        return true;
    }
}
