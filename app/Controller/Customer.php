<?php

namespace Racebet\Api\Controller;

use Racebet\Api\Http\Response;
use Symfony\Component\HttpFoundation\Request;
use Racebet\Api\Kernel\Service\Customer as CustomerService;

class Customer extends Base
{

    const KEY_STATUS = 'status';
    const KEY_MESSAGE = 'message';
    const KEY_ERROR_CODE = 'errorCode';

    /**
     * @param Request $request
     * @param CustomerService $customerService
     * @return Response
     */
    public function customersAction(Request $request, CustomerService $customerService) : Response
    {
        $result = $customerService->getCustomers();

        return new Response($result);
    }

    /**
     * Customer endpoint
     *
     * @param Request           $request
     * @param CustomerService   $customerService
     *
     * @return Response
     */
    public function customerAction(Request $request = null, CustomerService $customerService) : Response
    {
        $idCustomer = $request->get('id');
        $result = $customerService->getCustomer($idCustomer);

        return new Response($result);
    }


    /**
     * Customer endpoint
     *
     * @param Request           $request
     * @param CustomerService   $customerService
     *
     * @return Response
     */
    public function createCustomerAction(Request $request = null, CustomerService $customerService) : Response
    {
        $result = $customerService->createCustomer($request);

        return new Response($result);
    }

    /**
     * Update endpoint
     *
     * @param Request           $request
     * @param CustomerService   $customerService
     *
     * @return Response
     */
    public function updateCustomerAction(Request $request = null, CustomerService $customerService) : Response
    {
        $result = $customerService->updateCustomer($request);

        return new Response($result);
    }

}
