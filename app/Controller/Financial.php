<?php

namespace Racebet\Api\Controller;

use Racebet\Api\Http\Response;
use Symfony\Component\HttpFoundation\Request;
use Racebet\Api\Kernel\Service\Financial as FinancialService;

class Financial extends Base
{

    const KEY_STATUS     = 'status';
    const KEY_MESSAGE    = 'message';
    const KEY_ERROR_CODE = 'errorCode';

    /**
     * Deposit endpoint
     *
     * @param Request          $request
     * @param FinancialService $financialService
     *
     * @return Response
     */
    public function depositAction(Request $request = null, FinancialService $financialService) : Response
    {
        $result = $financialService->doDeposit($request);

        return new Response($result);
    }

    /**
     * Withdrawal endpoint
     *
     * @param Request          $request
     * @param FinancialService $financialService
     *
     * @return Response
     */
    public function withdrawalAction(Request $request = null, FinancialService $financialService) : Response
    {
        $result = $financialService->doWithdrawal($request);

        return new Response($result);
    }

    /**
     * Reports endpoint
     *
     * @param Request          $request
     * @param FinancialService $financialService
     *
     * @return Response
     */
    public function reportsAction(Request $request = null, FinancialService $financialService) : Response
    {
        $startDate = $request->get('start_date');
        $endDate   = $request->get('end_date');

        $result = $financialService->getReports($startDate, $endDate);

        return new Response($result);
    }

    /**
     * Reports endpoint
     *
     * @param Request          $request
     * @param FinancialService $financialService
     *
     * @return Response
     */
    public function defaultReportsAction(Request $request = null, FinancialService $financialService) : Response
    {
        $result = $financialService->getReports();
        return new Response($result);
    }

    /**
     * Customer balance endpoint
     *
     * @param Request          $request
     * @param FinancialService $financialService
     *
     * @return Response
     */
    public function getBalanceAction(Request $request = null, FinancialService $financialService) : Response
    {
        $idCustomer = $request->get('id_customer');

        $result = $financialService->getBalance($idCustomer);

        return new Response($result);
    }
}
