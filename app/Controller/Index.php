<?php

namespace Racebet\Api\Controller;

use Racebet\Api\Http\Response;
use Racebet\Api\Kernel\Service\Ping as PingService;

class Index extends Base
{

    const KEY_STATUS = 'status';
    const KEY_MESSAGE = 'message';
    const KEY_ERROR_CODE = 'errorCode';

    /**
     * Ping endpoint
     *
     * @param PingService $pingService
     *
     * @return Response
     */
    public function pingAction(PingService $pingService) : Response
    {
        return new Response($pingService->getOK());
    }
}
