<?php

namespace Racebet\Api\Kernel\Model;

class Customer
{
    const SQL_CREATE_CUSTOMER = "INSERT INTO customer (email, first_name, last_name, gender, country, bonus, created) VALUES (:email, :first_name, :last_name, :gender, :country, :bonus, now())";

    const SQL_UPDATE_CUSTOMER = "UPDATE customer set email=:email, first_name=:first_name, last_name=:last_name, gender=:gender, country=:country WHERE id_customer=:id_customer";

    const SQL_QUERY_CUSTOMERS = 'select id_customer, email, first_name, last_name, gender, country, bonus, created, updated from customer';
    const SQL_QUERY_CUSTOMER  = "select id_customer, email, first_name, last_name, gender, country, bonus, created, updated from customer where id_customer = ?";

    const SQL_CREATE_OPERATION = "INSERT INTO `operation` (`fk_customer`, `fk_operation_type`, `amount`, `bonus`, `created`)
VALUES (:id_customer, :id_operation_type, :amount, :bonus, now())";

    const SQL_CALCULATE_BALANCE = "select SUM(amount) as balance from operation  where fk_customer = :id_customer";

    const SQL_UPDATE_BALANCE = "INSERT INTO balance (fk_customer, amount, bonus, deposit, withdrawal, total_deposit, total_withdrawal, created) VALUES (:id_customer, :amount, :bonus, :deposit, :withdrawal, :sum_deposit, :sum_withdrawal, now()) ON DUPLICATE KEY UPDATE amount = amount + :amount, bonus = bonus + :bonus, deposit = deposit + :deposit, withdrawal = withdrawal + :withdrawal, total_deposit = total_deposit + :sum_deposit, total_withdrawal = total_withdrawal + :sum_withdrawal";

    const SQL_DEPOSITS = "select COUNT(id_operation) as deposits from operation  where fk_customer = :id_customer group by fk_operation_type";

    const SQL_REPORTS_DEFAULT = "select country, T.name, count(distinct O.fk_customer) as customers, sum(O.amount) as amount, count(O.id_operation) as operations, O.updated
from customer as C join balance as B on C.id_customer = B.fk_customer join operation as O on O.fk_customer = C.id_customer join operation_type as T on T.id_operation_type = O.fk_operation_type
where O.updated >= TIMESTAMP(DATE_SUB(now(), INTERVAL :days day)) group by country, O.fk_operation_type order by country, O.updated desc;";

    const SQL_REPORTS_TIME_FRAME2 = "select country, T.name, count(distinct O.fk_customer) as customers, sum(O.amount) as amount, count(O.id_operation) as operations, O.updated
from customer as C join balance as B on C.id_customer = B.fk_customer join operation as O on O.fk_customer = C.id_customer join operation_type as T on T.id_operation_type = O.fk_operation_type
where O.updated >= :start_date and O.updated <= :end_date group by country, O.fk_operation_type order by country, O.updated desc;";

    const SQL_REPORTS_TIME_FRAME = "select country, T.name, sum(O.amount) as amount, count(O.id_operation) as operations, O.updated 
from customer as C join balance as B on C.id_customer = B.fk_customer join operation as O on O.fk_customer = C.id_customer join operation_type as T on T.id_operation_type = O.fk_operation_type
where O.updated >= :start_date and O.updated <= :end_date group by country, O.fk_operation_type order by country, O.updated desc";

    const SQL_REPORTS_COUNT_CUSTOMERS_TIME_FRAME = "
select country, count(distinct O.fk_customer) as customers
from customer as C join balance as B on C.id_customer = B.fk_customer join operation as O on O.fk_customer = C.id_customer 
where O.updated >= :start_date and O.updated <= :end_date group by country;";

    const SQL_REPORTS_COUNT_CUSTOMERS_DEFAULT = "
select country, count(distinct O.fk_customer) as customers
from customer as C join balance as B on C.id_customer = B.fk_customer join operation as O on O.fk_customer = C.id_customer 
where O.updated >= TIMESTAMP(DATE_SUB(now(), INTERVAL :days day)) group by country;";

    const SQL_BALANCE = "select first_name, last_name, amount as balance, balance.bonus as 'bonus amount', deposit, withdrawal, total_deposit as 'deposit amount', total_withdrawal as 'withdrawal amount' from balance join customer on id_customer = fk_customer where fk_customer= :id_customer;";

    const DEPOSITS_NUMBER_BONUS = 3;
    const REPORTS_DEFAULT_TIME_FRAME = 7;
    const OPERATION_DEPOSIT = 1;
    const OPERATION_WITHDRAWAL = 2;

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var array
     */
    private $bonusSettings;

    /**
     * Customer constructor.
     *
     * @param \PDO $pdo
     * @param array $bonusSettings
     */
    public function __construct(\PDO $pdo, array $bonusSettings)
    {
        $this->pdo           = $pdo;
        $this->bonusSettings = $bonusSettings;
    }

    /**
     * @return array
     */
    public function getCustomers() : array
    {
        try {
            $statement = $this->pdo->prepare(self::SQL_QUERY_CUSTOMERS);
            $statement->execute();
            $customers = $statement->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $exception) {
            return null;
        }

        return $customers;
    }

    /**
     * @param int $idCustomer
     * @return array
     */
    public function getCustomer(int $idCustomer) : array
    {
        try {
            $statement = $this->pdo->prepare(self::SQL_QUERY_CUSTOMER);
            $statement->bindParam(self::OPERATION_DEPOSIT, $idCustomer, \PDO::PARAM_INT);
            $statement->execute();
            $customers = $statement->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $exception) {
            return false;
        }

        return (!empty($customers)) ? $customer = array_shift($customers) : [];
    }

    /**
     * @param $customer
     * @return bool
     */
    public function createCustomer($customer) : bool
    {
        try {
            $statement = $this->pdo->prepare(self::SQL_CREATE_CUSTOMER);
            $statement->bindParam(':email', $customer['email']);
            $statement->bindParam(':first_name', $customer['first_name']);
            $statement->bindParam(':last_name', $customer['last_name']);
            $statement->bindParam(':gender', $customer['gender']);
            $statement->bindParam(':country', $customer['country']);
            $rand = rand($this->bonusSettings['min'], $this->bonusSettings['max']);
            $statement->bindParam(':bonus', $rand);

            return $statement->execute();
        } catch (\PDOException $ex) {
            return false;
        }
    }

    /**
     * @param $customer
     * @return bool
     */
    public function updateCustomer($customer) : bool
    {
        try {
            $statement = $this->pdo->prepare(self::SQL_UPDATE_CUSTOMER);
            $statement->bindParam(':id_customer', $customer['id_customer'], \PDO::PARAM_INT);
            $statement->bindParam(':email'      , $customer['email']);
            $statement->bindParam(':first_name' , $customer['first_name']);
            $statement->bindParam(':last_name'  , $customer['last_name']);
            $statement->bindParam(':gender'     , $customer['gender']);
            $statement->bindParam(':country'    , $customer['country']);

            return $statement->execute();
        } catch (\PDOException $ex) {
            return false;
        }
    }

    /**
     * @param array $operation
     * @return bool
     */
    public function doDeposit(array $operation) : bool
    {
        $type = self::OPERATION_DEPOSIT;
        try {
            $this->pdo->beginTransaction();

            $amount = (int)$operation['amount'];
            $bonus = $this->getBonus($operation, $amount);

            $statement = $this->pdo->prepare(self::SQL_CREATE_OPERATION);
            $statement->bindParam(':id_customer', $operation['id_customer'], \PDO::PARAM_INT);
            $statement->bindParam(':id_operation_type', $type, \PDO::PARAM_INT);
            $statement->bindParam(':amount', $amount, \PDO::PARAM_INT);
            $statement->bindParam(':bonus' , $bonus, \PDO::PARAM_INT);
            $statement->execute();

            $updateBalance = $this->updateBalance($operation['id_customer'], $amount , $bonus, 1);

            if ($updateBalance === false) {
                $this->pdo->rollBack();
                return false;
            }

            $this->pdo->commit();
            return true;
        } catch (\PDOException $ex) {
            $this->pdo->rollBack();
            return false;
        }
    }

    /**
     * @param array $operation
     *
     * @return bool
     */
    public function doWithdrawal(array $operation) : bool
    {
        $type = self::OPERATION_WITHDRAWAL;

        try {
            $this->pdo->beginTransaction();

            $balance = $this->calculateBalance($operation['id_customer']);

            if ($balance - $operation['amount'] < 0) {
                return false;
            }

            $amount = -$operation['amount'];
            $bonus = 0;

            $statement = $this->pdo->prepare(self::SQL_CREATE_OPERATION);
            $statement->bindParam(':id_customer', $operation['id_customer'], \PDO::PARAM_INT);
            $statement->bindParam(':id_operation_type', $type, \PDO::PARAM_INT);
            $statement->bindParam(':amount' , $amount, \PDO::PARAM_INT);
            $statement->bindParam(':bonus' , $bonus, \PDO::PARAM_INT);

            if ($statement->execute() === 0 ) {
                $this->pdo->rollBack();
                return false;
            }

            $updateBalance = $this->updateBalance($operation['id_customer'], $amount, 0, 0, 1);

            if ($updateBalance === false) {
                $this->pdo->rollBack();
                return false;
            }

            $this->pdo->commit();
            return true;
        } catch (\PDOException $ex) {
            $this->pdo->rollBack();
            return false;
        }
    }

    /**
     * @param string|null $startDate
     * @param string|null $endDate
     *
     * @return array
     */
    public function reportsWithCustomers(string $startDate = null, string $endDate = null) : array
    {
        $reports = $this->reports($startDate, $endDate);
        $countCustomers = $this->countCustomersPerCountry($startDate, $endDate);

        return $this->buildReports($reports, $countCustomers);
    }

    /**
     * @param string|null $startDate
     * @param string|null $endDate
     *
     * @return array
     */
    public function reports(string $startDate = null, string $endDate = null) : array
    {
        try {
            $statement = $this->prepareReportsStatement($startDate, $endDate);
            $statement->execute();
            $reports = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $reports = $this->parseIntValues($reports);
        } catch (\PDOException $exception) {
            return null;
        }

        return $reports;
    }

    /**
     * @param string|null $startDate
     * @param string|null $endDate
     * 
     * @return \PDOStatement
     */
    public function prepareReportsStatement(string $startDate = null, string $endDate = null) : \PDOStatement
    {
        if (is_null($startDate) || is_null($endDate)) {
            $days = self::REPORTS_DEFAULT_TIME_FRAME;
            $statement = $this->pdo->prepare(self::SQL_REPORTS_DEFAULT);
            $this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
            $statement->bindParam(':days', $days, \PDO::PARAM_STR);
        } else {
            $statement = $this->pdo->prepare(self::SQL_REPORTS_TIME_FRAME);
            $this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
            $statement->bindParam(':start_date', $startDate, \PDO::PARAM_STR);
            $statement->bindParam(':end_date', $endDate, \PDO::PARAM_STR);
        }
        
        return $statement;
    }


    /**
     * @param string|null $startDate
     * @param string|null $endDate
     *
     * @return array
     */
    public function countCustomersPerCountry(string $startDate = null, string $endDate = null) : array
    {
        try {
            $statement = $this->prepareCountCustomersStatement($startDate, $endDate);
            $statement->execute();
            $reports = $statement->fetchAll(\PDO::FETCH_KEY_PAIR);

            $reports = $this->parseIntValues($reports);
        } catch (\PDOException $exception) {
            return null;
        }

        return $reports;
    }

    /**
     * @param string|null $startDate
     * @param string|null $endDate
     *
     * @return \PDOStatement
     */
    public function prepareCountCustomersStatement(string $startDate = null, string $endDate = null) : \PDOStatement
    {
        if (is_null($startDate) || is_null($endDate)) {
            $days = self::REPORTS_DEFAULT_TIME_FRAME;
            $statement = $this->pdo->prepare(self::SQL_REPORTS_COUNT_CUSTOMERS_DEFAULT);
            $this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
            $statement->bindParam(':days', $days, \PDO::PARAM_STR);
        } else {
            $statement = $this->pdo->prepare(self::SQL_REPORTS_COUNT_CUSTOMERS_TIME_FRAME);
            $this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
            $statement->bindParam(':start_date', $startDate, \PDO::PARAM_STR);
            $statement->bindParam(':end_date', $endDate, \PDO::PARAM_STR);
        }

        return $statement;
    }

    /**
     * @param int $idCustomer
     * @return array
     */
    public function getBalance(int $idCustomer) : array
    {
        try {

            $statement = $this->pdo->prepare(self::SQL_BALANCE);
            $this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
            $statement->bindParam(':id_customer' , $idCustomer, \PDO::PARAM_INT);

            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $balance = array_shift($results);
            $balance = $this->parseIntValues($balance);

            $balance['balance']           = $this->getCents($balance['balance']);
            $balance['bonus amount']      = $this->getCents($balance['bonus amount']);
            $balance['deposit amount']    = $this->getCents($balance['deposit amount']);
            $balance['withdrawal amount'] = $this->getCents($balance['withdrawal amount']);

        } catch (\PDOException $exception) {
            return null;
        }

        return $balance;
    }

    /**
     * @param array $reports
     * @param array $countCustomers
     *
     * @return array
     */
    private function buildReports(array $reports, array $countCustomers) : array
    {
        $finalReports = [];
        foreach ($reports as $report) {
            $rowReport = isset($finalReports[$report['country']]) ?
                $finalReports[$report['country']] :
                [
                    'Country'                  => null,
                    'Unique Customers'         => 0,
                    'No of Deposits'           => 0,
                    'Total Deposit Amount'     => 0,
                    'No of Withdrawals'        => 0,
                    'Total Withdrawals Amount' => 0,
                ];

            $rowReport['Country'] = $report['country'];
            $rowReport['Unique Customers'] = $countCustomers[$report['country']];

            if ($report['name'] == 'deposit') {
                $rowReport['No of Deposits']       += $report['operations'];
                $rowReport['Total Deposit Amount'] += $this->getCents($report['amount']);
            } else {
                $rowReport['No of Withdrawals']        +=  $report['operations'];
                $rowReport['Total Withdrawals Amount'] += $this->getCents($report['amount']);
            }
            $finalReports [$report['country']] = $rowReport;
        }

        return $finalReports;
    }

    /**
     * @param int $idCustomer
     *
     * @return string
     */
    private function calculateBalance(int $idCustomer) : string
    {
        $statement = $this->pdo->prepare(self::SQL_CALCULATE_BALANCE);
        $statement->bindParam(':id_customer', $idCustomer, \PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchColumn(0);

        return (int)$result;
    }

    /**
     * @param int $idCustomer
     *
     * @return string
     */
    private function countDeposits(int $idCustomer) : string
    {
        $statement = $this->pdo->prepare(self::SQL_DEPOSITS);
        $statement->bindParam(':id_customer', $idCustomer, \PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchColumn(0);

        return (int)$result;
    }


    /**
     * @param int $idCustomer
     * @param int $amount
     * @param int $bonus
     * @param int $deposit
     * @param int $withdrawal
     *
     * @return bool
     */
    private function updateBalance(int $idCustomer, int $amount, int $bonus, int $deposit= 0, int $withdrawal= 0) : bool
    {
        list($sumDeposit, $sumWithdrawal) = ($amount > 0) ? [$amount, 0] : [0, $amount];

        $statement = $this->pdo->prepare(self::SQL_UPDATE_BALANCE);
        $statement->bindParam(':id_customer', $idCustomer, \PDO::PARAM_INT);
        $statement->bindParam(':amount', $amount, \PDO::PARAM_INT);
        $statement->bindParam(':bonus', $bonus, \PDO::PARAM_INT);
        $statement->bindParam(':deposit', $deposit, \PDO::PARAM_INT);
        $statement->bindParam(':withdrawal', $withdrawal, \PDO::PARAM_INT);
        $statement->bindParam(':sum_withdrawal', $sumWithdrawal, \PDO::PARAM_INT);
        $statement->bindParam(':sum_deposit', $sumDeposit, \PDO::PARAM_INT);


        return $statement->execute();
    }

    /**
     * @param array $operation
     * @param int $deposits
     * @param array $customer
     *
     * @return int
     */
    private function calculateBonus(array $operation, int $deposits, array $customer) : int
    {
        return ($deposits % (self::DEPOSITS_NUMBER_BONUS) == self::OPERATION_WITHDRAWAL) ?
            (int)$operation['amount'] * ($customer['bonus'] / 100) :
            0;
    }

    /**
     * @param array $reports
     *
     * @return array
     */
    private function parseIntValues(array $reports) : array
    {
        foreach ($reports as $keyReports => $report) {
            if (is_array($report)){
                foreach ($report as $key => $detail) {
                    $report[ $key ] = $this->transformToInt($detail);
                    $reports[ $keyReports ] = $report;
                }
            } else {
                $reports[ $keyReports ] = $this->transformToInt($report);
            }
        }

        return $reports;
    }

    /**
     * @param $value
     *
     * @return int|string
     */
    private function transformToInt($value)
    {
        return (is_numeric($value)) ? $value + 0 : $value;
    }

    /**
     * @param array $operation
     * @param int $amount
     * @return int
     */
    private function getBonus(array $operation, $amount) : int
    {
        if ($amount > 0) {
            $deposits = $this->countDeposits($operation['id_customer']);
            $customer = $this->getCustomer($operation['id_customer']);
            $bonus = $this->calculateBonus($operation, $deposits, $customer);
            return $bonus;
        }

        return 0;
    }

    /**
     * @param int $amount
     *
     * @return float
     */
    private function getCents(int $amount) : float
    {
        return (float) $amount / 100;
    }
}
