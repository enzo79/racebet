<?php

namespace Racebet\Api\Kernel\Entity;

/**
 * Class Customer
 * @package Racebet\Api\Kernel\Model\Entity
 */
class Customer
{
    /**
     * @var int
     */
    public $id_customer;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;

    /**
     * @var string
     */
    public $gender;

    /**
     * @var string
     */
    public $country;

    /**
     * @return int
     */
    public function getIdCustomer() : int
    {
        return $this->id_customer;
    }

    /**
     * @param int $id_customer
     * @return Customer
     */
    public function setIdCustomer($id_customer) : Customer
    {
        $this->id_customer = $id_customer;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Customer
     */
    public function setEmail($email) : Customer
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName() : string
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     * @return Customer
     */
    public function setFirstName($first_name) : Customer
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName() : string
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     * @return Customer
     */
    public function setLastName($last_name) : Customer
    {
        $this->last_name = $last_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender() : string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return Customer
     */
    public function setGender($gender) : Customer
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry() : string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return Customer
     */
    public function setCountry($country) : Customer
    {
        $this->country = $country;
        return $this;
    }
}
