<?php
namespace Racebet\Api\Kernel\Service;

use Symfony\Component\HttpFoundation\Request;
use Racebet\Api\Kernel\Model\Customer as CustomerModel;
use Racebet\Api\Kernel\Validator\Financial as FinancialValidator;
use Racebet\Api\Kernel\Service\Base as BaseService;

class Financial extends BaseService
{
    const RESULTS = 'reports';
    const REQUEST_MAIN_OBJECT = 'operation';
    const ERROR_DAYS = 'parameters start_date and end_date must be validate dates';

    /**
     * @param CustomerModel      $customer
     * @param FinancialValidator $validator
     */
    public function __construct(CustomerModel $customer, FinancialValidator $validator)
    {
        parent::__construct($customer);
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function doDeposit(Request $request) : array
    {
        if (!$operation = $this->extractMainObject($request)) {
            return $this->prepareErrorResponse(['Request has a wrong format, object ' . static::REQUEST_MAIN_OBJECT] .
            'missing');
        }

        $violations = $this->validator->validate($operation);

        if (is_array($errors = $this->getViolationErrors($violations) )) {
            return $errors;
        }

        try {
            if ($this->customerModel->doDeposit($operation) === true) {
                return $this->prepareResponse(null, self::SUCCESS_CODE, 'Deposit done');
            } else {
                return $this->prepareErrorResponse(['It\'s not possible to perform a deposit']);
            }

        } catch (\Exception $e) {
            return $this->prepareErrorResponse([$e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function doWithdrawal(Request $request) : array
    {
        if (!$operation = $this->extractMainObject($request)) {
            return $this->prepareErrorResponse(['Request has a wrong format, object ' . static::REQUEST_MAIN_OBJECT]);
        }

        $violations = $this->validator->validate($operation);
        if (is_array($errors = $this->getViolationErrors($violations) )) {
            return $errors;
        }

        try {
            if ($this->customerModel->doWithdrawal($operation) === true) {
                return $this->prepareResponse(null, self::SUCCESS_CODE, 'Withdrawal done');
            } else {
                return $this->prepareErrorResponse(['It\'s possible to perform a withdrawal']);
            }

        } catch (\Exception $e) {
            return $this->prepareErrorResponse([$e->getMessage()]);
        }
    }

    /**
     * @param string|null $startDate
     * @param string|null $endDate
     *
     * @return array
     */
    public function getReports(string $startDate = null, string $endDate = null) : array
    {

        if ((is_null($startDate) || strtotime($startDate) !== false) &&
            (is_null($endDate) || strtotime($endDate) !== false) ) {
            $reports = $this->customerModel->reportsWithCustomers($startDate, $endDate);
            $response = $this->prepareResponse([self::RESULTS_COUNT => count($reports), 'results' => $reports]);
        } else {
            $response = $this->prepareErrorResponse([self::ERROR_DAYS]);
        }
        
        return $response;
    }

    /**
     * @param int $idCustomer
     * @return array
     */
    public function getBalance(int $idCustomer) : array
    {
        
        if (is_numeric($idCustomer)) {
            $balance = $this->customerModel->getBalance($idCustomer);
            $response = $this->prepareResponse(['balance' => $balance]);
        } else {
            $response = $this->prepareErrorResponse([self::ERROR_CUSTOMER_ID]);
        }
        
        return $response;
    }
}
