<?php
namespace Racebet\Api\Kernel\Service;

use Symfony\Component\HttpFoundation\Request;
use \Racebet\Api\Kernel\Model\Customer as CustomerModel;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;

abstract class Base
{
    const ERROR_CODE = 'v100';

    const RESULTS = 'Result';
    const RESULTS_COUNT = 'count';

    const KEY_STATUS = 'status';
    const KEY_MESSAGE = 'message';

    const SUCCESS_CODE = 'Ok';
    const STATUS_ERROR = 'error';

    const REQUEST_MAIN_OBJECT = null;

    const ERROR_CUSTOMER_ID = 'parameter customer_id must be an integer';

    /**
     * @var CustomerMode
     */
    protected $customerModel;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * Customer constructor.
     * @param CustomerModel $customer
     */
    public function __construct(CustomerModel $customer)
    {
        $this->customerModel = $customer;
    }

    /**
     * @param $count
     * @param array $results
     * @return array
     */
    protected function createResponse($count, array $results) : array
    {
        return [
            'count' => $count,
            'results' => $results
        ];
    }

    /**
     * @param array $result
     * @param string $status
     * @param string $message
     * @return array
     */
    protected function prepareResponse(array $result = null, $status = self::SUCCESS_CODE, $message = '') : array
    {
        $response = [
            static::KEY_STATUS => $status,
            static::KEY_MESSAGE    => $message,
        ];

        return empty($result) ? $response : array_merge($response, [static::RESULTS => $result]);
    }

    /**
     * @param array $messages
     * @return array
     */
    protected function prepareErrorResponse(array $messages) : array
    {
        $response = [
            static::KEY_STATUS => self::STATUS_ERROR,
            'errors'    => $messages,
        ];

        return $response;
        //return $this->prepareResponse(null, self::STATUS_ERROR, $messages);
    }

    /**
     * @param null $code
     * @param null $message
     * @return array
     */
    protected function prepareErrorMessage($code = null, $message = null) : array
    {
        return [
            'code' => ($code) ? $code : self::ERROR_CODE,
            'messages' => ($message) ? $message : self::KEY_MESSAGE];
    }

    /**
     * @param ConstraintViolationList $violations
     * @return array
     */
    protected function getViolationsMessages(ConstraintViolationList $violations) : array
    {
        $messages = [];
        foreach ($violations as $violation) {
            $messages[] = $violation->getMessage() .' '. $violation->getPropertyPath();
        }
        return $messages;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function extractMainObject(Request $request)
    {
        return $request->request->get(static::REQUEST_MAIN_OBJECT);
    }

    /**
     * @param $violations
     * @return array|null
     */
    protected function getViolationErrors($violations)
    {
        return ($violations->count() > 0) ?
            $this->prepareErrorResponse($this->prepareErrorMessage(
                self::ERROR_CODE, $this->getViolationsMessages($violations))
            ) : null;
    }
}
