<?php
namespace Racebet\Api\Kernel\Service;

use Symfony\Component\HttpFoundation\Request;
use Racebet\Api\Kernel\Model\Customer as CustomerModel;
use Racebet\Api\Kernel\Service\Base as BaseService;
use Racebet\Api\Kernel\Validator\Customer as CustomerValidator;

class Customer extends BaseService
{

    const RESULTS = 'customers';
    const REQUEST_MAIN_OBJECT = 'customer';

    /**
     * @param CustomerModel $customer
     * @param CustomerValidator $validator
     */
    public function __construct(CustomerModel $customer, CustomerValidator $validator)
    {
        parent::__construct($customer);
        $this->validator = $validator;
    }

    /**
     * @return array
     */
    public function getCustomers() : array
    {
        $customers = $this->customerModel->getCustomers();
        return $this->prepareResponse([self::RESULTS_COUNT => count($customers), 'results' => $customers]);
    }

    /**
     * @param $idCustomer
     * @return array
     */
    public function getCustomer($idCustomer) : array
    {
        if (is_numeric($idCustomer)) {
            $response = $this->prepareResponse($this->customerModel->getCustomer($idCustomer));
        } else {
            $response = $this->prepareErrorResponse([self::ERROR_CUSTOMER_ID]);
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function createCustomer(Request $request) : array
    {
        if (!$customer = $this->extractMainObject($request)) {
            return $this->prepareErrorResponse(['Request has a wrong format, object ' . static::REQUEST_MAIN_OBJECT]);
        }

        $violations = $this->validator->validateCreate($customer);
        if (is_array($errors = $this->getViolationErrors($violations) )) {
            return $errors;
        }

        try {

            if ($this->customerModel->createCustomer($customer) === true) {
                return $this->prepareResponse(null, self::SUCCESS_CODE, 'Customer created ');
            } else {
                return $this->prepareErrorResponse(['No Customer created ']);
            }

        } catch (\Exception $e) {
            return $this->prepareErrorResponse([$e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateCustomer(Request $request) : array
    {
        $customer = $this->extractMainObject($request);
        $violations = $this->validator->validateUpdate($customer);

        if (is_array($errors = $this->getViolationErrors($violations) )) {
            return $errors;
        }

        try {

            if ($this->customerModel->updateCustomer($customer) === true) {
                return $this->prepareResponse(null, self::SUCCESS_CODE, 'Customer updated');
            } else {
                return $this->prepareErrorResponse(['No Customer updated ']);
            }

        } catch (\Exception $e) {
            return $this->prepareErrorResponse([$e->getMessage()]);
        }
    }
}
