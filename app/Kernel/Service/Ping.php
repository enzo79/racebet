<?php

namespace Racebet\Api\Kernel\Service;

class Ping
{
    const KEY_STATUS     = 'status';
    const KEY_MESSAGE    = 'message';
    const KEY_ERROR_CODE = 'errorCode';

    /**
     * Get "ok"
     *
     * @return string
     */
    public function getOK()
    {
        return [
            self::KEY_STATUS     => 'ok',
            self::KEY_MESSAGE    => '',
        ];
    }
}
