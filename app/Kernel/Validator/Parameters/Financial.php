<?php

namespace Racebet\Api\Kernel\Validator\Parameters;

interface Financial
{
    const ID_CUSTOMER = 'id_customer';
    const OPERATION   = 'operation';
    const AMOUNT      = 'amount';
}
