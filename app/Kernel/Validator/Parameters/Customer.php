<?php

namespace Racebet\Api\Kernel\Validator\Parameters;

interface Customer
{
    const ID_CUSTOMER = 'id_customer';
    const EMAIL       = 'email';
    const FIRST_NAME  = 'first_name';
    const LAST_NAME   = 'last_name';
    const GENDER      = 'gender';
    const COUNTRY     = 'country';
}
