<?php

namespace Racebet\Api\Kernel\Validator;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface as ViolationList;

/**
 * Class Customer
 * @package Racebet\Api\Kernel\Validator
 */
class Customer implements \Racebet\Api\Kernel\Validator\Parameters\Customer
{
    const CREATE_RULES = 'create';
    const UPDATE_RULES = 'update';

    /**
     * Validator
     */
    protected $validator;

    /**
     * Constructor
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return ViolationList
     */
    public function validateCreate(array $data) : ViolationList
    {
        $validationRules = $this->getValidationRules(self::CREATE_RULES);
        $violationsList = $this->validator->validate($data, $validationRules);

        return $violationsList;
    }

    /**
     * @param array $data
     * @return ViolationList
     */
    public function validateUpdate(array $data) : ViolationList
    {
        $validationRules = $this->getValidationRules(self::UPDATE_RULES);
        $violationsList = $this->validator->validate($data, $validationRules);

        return $violationsList;
    }



    /**
     * @return Assert\Collection
     */
    private function getValidationRules(string $type) : Assert\Collection
    {
        switch ($type){
            case self::CREATE_RULES:
                return new Assert\Collection($this->validationRulesCreate());

            case self::UPDATE_RULES:
                return new Assert\Collection($this->validationRulesUpdate());
        }

    }


    /**
     * @return array
     */
    private function getFirstNameAssert() : array
    {
        return [new Assert\NotBlank()];
    }

    /**
     * @return array
     */
    private function getLastNameAssert() : array
    {
        return [new Assert\NotBlank()];
    }

    /**
     * @return array
     */
    private function validationRulesCreate()
    {
        return [
            'allowExtraFields' => true,
            'fields'           => [
                self::EMAIL      => [new Assert\NotBlank(), new Assert\Email()],
                self::FIRST_NAME => $this->getFirstNameAssert(),
                self::LAST_NAME  => $this->getLastNameAssert(),
                self::GENDER     => new Assert\Optional(new Assert\Choice(['Male', 'Female'])),
                self::COUNTRY    => [new Assert\NotBlank(), new Assert\Country()],
            ]
        ];
    }

    /**
     * @return array
     */
    private function validationRulesUpdate()
    {
        return [
            'allowExtraFields' => true,
            'fields'           => [
                self::ID_CUSTOMER => [new Assert\NotBlank(), new Assert\Type('int')],
                self::EMAIL       => [new Assert\NotBlank(), new Assert\Email()],
                self::FIRST_NAME  => $this->getFirstNameAssert(),
                self::LAST_NAME   => $this->getLastNameAssert(),
                self::GENDER      => new Assert\Optional(new Assert\Choice(['Male', 'Female'])),
                self::COUNTRY     => [new Assert\NotBlank(), new Assert\Country()],
            ]
        ];
    }
}
