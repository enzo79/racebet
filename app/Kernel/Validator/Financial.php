<?php

namespace Racebet\Api\Kernel\Validator;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface as ViolationList;
use \Racebet\Api\Kernel\Validator\Parameters\Financial as FinancialParameters;

/**
 * Class Customer
 * @package Racebet\Api\Kernel\Validator
 */
class Financial implements FinancialParameters
{
    /**
     * Validator
     */
    protected $validator;

    /**
     * Constructor
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return ViolationList
     */
    public function validate(array $data) : ViolationList
    {
        $validationRules = $this->getValidationRules();
        $violationsList = $this->validator->validate($data, $validationRules);
        return $violationsList;
    }

    /**
     * @return Assert\Collection
     */
    private function getValidationRules() : Assert\Collection
    {
        $rules = [
            'allowExtraFields' => true,
            'fields'           => [
                self::ID_CUSTOMER => [new Assert\NotBlank(), new Assert\Type('int')],
                self::OPERATION   => new Assert\Optional(new Assert\Choice(['deposit', 'withdrawal'])),
                self::AMOUNT      => [new Assert\NotBlank(), new Assert\Type('int')],
            ]
        ];

        return new Assert\Collection($rules);
    }
}
